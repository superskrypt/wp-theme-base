export default class TemplateRenderer {
    static getTemplate(templateId, templateData = {}, skipCache = false) {
        return !skipCache ? TemplateRenderer.getTemplateFromCacheAsPromise(templateId) || TemplateRenderer.fetchTemplate(templateId, templateData) : TemplateRenderer.fetchTemplate(templateId, templateData);
    }

    static getTemplateFromCacheAsPromise(templateId) {
        const template = JSON.parse(localStorage.getItem(templateId));
        return (template && template.ver !== undefined) ? Promise.resolve( template.data ) : null;
    }

    static fetchTemplate(templateId, templateData) {
        console.log('fetchTemplate', templateId, templateData);
        console.log('LN');
        return fetch('/wp-admin/admin-ajax.php?action=bb_get_template', {
            method: 'POST',
            headers: {
                "Content-Type": "application/html",
            },
            body: JSON.stringify({
                template_id: templateId,
                template_data: templateData,
            })
        })
        .then(response => response.text())
        .then(responseString => {
            const response = JSON.parse(responseString);
            if (response.success) {
                delete response.success;
                localStorage.setItem(templateId, JSON.stringify(response));
                return response.data;
            } else {
                return Promise.reject(response.data || 'Unknown error fetching template');
            }
        });
    }

    static createElementFromTemplate(template) {
        const templateNode = document.createElement('template');
        templateNode.innerHTML = template;
        return templateNode.content.children[0];
    }
}