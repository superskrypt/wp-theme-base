<?php
namespace Superskrypt\WpThemeBase\TemplateRenderer;

use Timber\Timber;

class TemplateRenderer {
    private static array $templateMapping;

    public static function init($templateMapping) {
        add_action('wp_ajax_bb_get_template', [__CLASS__, 'getTemplate']);
        add_action('wp_ajax_nopriv_bb_get_template', [__CLASS__, 'getTemplate']);
        self::$templateMapping = $templateMapping;
    }
    
    public static function getTemplate() {
        $dataRaw = file_get_contents('php://input');
        $data = json_decode($dataRaw);

        if (!self::$templateMapping) {
            wp_send_json_error('Template mapping not configured');
        }
        if (!$data) {
            wp_send_json_error('Invalid data');
        }

        $template_id = $data->template_id ?? NULL;
        $template_data = (array)$data->template_data ?? [];
        // error_log('Template ID: ' . $template_id   $template_data);
        error_log(print_r($template_data, true));
        if (!$template_id) {
            wp_send_json_error('Template ID not provided');
        }

        $template = self::$templateMapping[$template_id] ?? NULL;
        if ($template) {
            $context = Timber::get_context();   
            $context = [...$context, ...$template_data];
            wp_send_json([
                'success' => true,
                'ver' => \Superskrypt\WpThemeBase\WpThemeBase::getVersionEnvironment(),
                'data' => trim(Timber::compile($template, $context)),
            ]);
        } else {
            wp_send_json_error('Template not found');
        }
    }
}