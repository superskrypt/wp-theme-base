<?php

namespace Superskrypt\WpThemeBase;

class SsCustomPageTemplate {
    private $params;

    public function __construct($params) {
        $this->params = $params;
        $this->hooks();
    }

    private function hooks() {
        add_action('admin_menu', array($this, 'superskrypt_custom_page_menu_link'));
        add_action('admin_init', array($this, 'superskrypt_modify_editor_view_on_custom_page'));
        add_filter('save_post', array($this, 'superskrypt_save_post_meta_on_new_custom_page'), 1, 3);
        add_filter('display_post_states', array($this, 'superskrypt_custom_page_post_state'), 10, 2);
    }

    public function superskrypt_custom_page_menu_link() {
        $url = esc_url(add_query_arg($this->params['slug'], '1', 'post-new.php?post_type=page'));
        add_submenu_page('edit.php?post_type=page', sprintf(__('Dodaj stronę %s', 'superskrypt'), $this->params['label']), sprintf(__('Dodaj stronę %s', 'superskrypt'), $this->params['label']), 'publish_pages', $url);
    }

    public function superskrypt_modify_editor_view_on_custom_page() {
        if (isset($_GET['post'])) {
            $postID = $_GET['post'];
        } else if (isset($_POST['post_ID'])) {
            $postID = $_POST['post_ID'];
        } else if (isset($_GET['from_post'])) {
            $postID = $_GET['from_post'];
        }

        if (!isset($postID) || empty($postID)) {
            return;
        }

        if ($this->superskrypt_is_custom_page($postID)) {
            global $wp_post_types;

            $wp_post_types['page']->labels->edit_item = sprintf(__('Edytuj stronę %s', 'superskrypt'), $this->params['label']);
            $wp_post_types['page']->labels->add_new = sprintf(__('Dodaj nową stronę %s', 'superskrypt'), $this->params['label']);
            $wp_post_types['page']->labels->add_new_item = sprintf(__('Dodaj nową stronę %s', 'superskrypt'), $this->params['label']);
            add_action('admin_head', array($this, 'superskrypt_modify_add_new_link_on_custom_page'));
            remove_post_type_support('page', 'editor');
        }
    }

    public function superskrypt_save_post_meta_on_new_custom_page($postID, $post, $update) {
        if (!isset($_GET[$this->params['slug']])) {
            return;
        }
        $custom_page = $_GET[$this->params['slug']];
        if ($custom_page) {

            update_post_meta($postID, '_wp_page_template', 'page-' . $this->params['slug'] . '.php');
            remove_post_type_support('page', 'editor');
        }
    }

    function superskrypt_custom_page_post_state($states, $post) {
        if ($this->superskrypt_is_custom_page($post->ID)) {
            $states[] = $this->params['label'];
        }
        return $states;
    }

    public function superskrypt_modify_add_new_link_on_custom_page() {
        global $post, $post_new_file;
        if ($post_new_file) {
            $post_new_file = add_query_arg($this->params['slug'], 1, $post_new_file);

        }
    }


    public function superskrypt_is_custom_page($postID = 0) {
        if (get_post_meta($postID, '_wp_page_template', true) == 'page-' . $this->params['slug'] . '.php') {
            return true;
        }

        return false;
    }

}


class SsCustomPageNewTemplate extends SsCustomPageTemplate {

    public function __construct($params) {
        parent::__construct($params);
        $this->hooks();
    }

    private function hooks() {
        add_action('save_post', array($this, 'ss_custom_page_merge_to_content'));
    }

    public function ss_custom_page_merge_to_content($postID) {

        if (!$this->superskrypt_is_custom_page($postID)) {
            return;
        }

        $content = '';

        $to_update = array(
            'ID' => $postID,
            'post_content' => $content,
        );

        remove_action('save_post', array($this, 'ss_custom_page_merge_to_content'));
        wp_update_post($to_update);
        add_action('save_post', array($this, 'ss_custom_page_merge_to_content'));
    }

}

$i = 0;
foreach ($templates as $slug => $label) {
	$args = array(
	    'slug' => $slug,
	    'label' => __($label, 'ss'),
	);
	${'custom_template' . $i}= new SsCustomPageTemplate($args);
	$i++;
}
