<?php
namespace Superskrypt\WpThemeBase\Setup;

class ThemeCreator {

    public static function registerScripts( $scripts ) {
        $path_to_js = get_template_directory_uri() . '/../js/';

        foreach ( $scripts as $script ) {
            $handle = is_array($script) ? $script['handle'] : $script;
            $src = isset($script['src']) ? $script['src'] : (isset($script['file_name']) ? $path_to_js . $script['file_name'] . '.js' : $path_to_js . $handle . '.js');
            $deps = isset($script['deps']) ? $script['deps'] : [];
            $ver = isset($script['src']) ? '' : \Superskrypt\WpThemeBase\WpThemeBase::getVersionEnvironment();
            $in_footer = isset($script['in_footer']) ? $script['in_footer'] : true;

            wp_register_script($handle, $src, $deps, $ver, $in_footer);
        }
    }

    public static function registerStyles( $styles ) {
        $path_to_css = get_template_directory_uri() . '/../css/';

        foreach ( $styles as $style ) {
            $handle = is_array($style) ? $style['handle'] : $style;
            $src = isset($style['src']) ? $style['src'] : (isset($style['file_name']) ? $path_to_css . $style['file_name'] . '.css' : $path_to_css . $handle . '.css');
            $deps = isset($style['deps']) ? $style['deps'] : [];
            $ver = isset($style['src']) ? '' : \Superskrypt\WpThemeBase\WpThemeBase::getVersionEnvironment();
            $media = isset($style['media']) ? $style['media'] : 'all';

            wp_register_style($handle, $src, $deps, $ver, $media);
        }
	}

    public static function addPageTitleSchemeFilter() {
        add_filter( 'wp_title', function ( $title ) {
            if ( empty( $title ) && ( is_home() || is_front_page() ) ) {
                return get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' );
            }
            if ( is_page() && (!is_home() && !is_front_page()) ) {
                return $title . ' | ' . get_bloginfo( 'name' );
            }
            return $title;
        });
    }
}
