<?php
namespace Superskrypt\WpThemeBase\Setup;

class WpCleaner {
    public static function init() {
        remove_filter( 'render_block', 'wp_render_layout_support_flag', 10, 2 );
        add_action( 'wp_enqueue_scripts', array(__CLASS__, 'deregisterWpStyles'), 100 );
        add_action( 'wp_enqueue_scripts', array(__CLASS__, 'deregisterWpmlScripts'), 100 );
    }

    public static function deregisterWpStyles() {
        global $wp_styles;
    
        foreach ( $wp_styles->queue as $key => $handle ) {
            if ( strpos( $handle, 'wp-block-' ) === 0 ) {
                wp_dequeue_style( $handle );
            }
        }
        wp_dequeue_style( 'global-styles' );
        wp_dequeue_style( 'classic-theme-styles' );

    }
    public static function deregisterWpmlScripts() {
        if(!is_admin()) {
            wp_deregister_style( 'wpml-tm-admin-bar');
            wp_deregister_style( 'wpml-blocks');
            wp_deregister_script( 'wpml-domain-validation');
            wp_deregister_script( 'wpml-underscore-template-compiler');
        }
        if(!defined('ICL_DONT_LOAD_NAVIGATION_CSS')) {
            define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
        }
        if(!defined('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS')) {
            define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
        }
        if(!defined('ICL_DONT_LOAD_LANGUAGES_JS')) {
            define('ICL_DONT_LOAD_LANGUAGES_JS', true);
        }
    }
}
