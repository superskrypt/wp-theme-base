<?php

namespace Superskrypt\WpThemeBase\Setup;

class TimberCarbonFieldsIntegration {

    public static function init() {
        add_filter('timber_post_get_meta_field', function ($value, $id, $field_name) {
            return carbon_get_post_meta($id, $field_name);
        }, 10, 3);

        add_filter('timber/term/meta/field', function ($value, $id, $field_name) {
            return carbon_get_term_meta($id, $field_name);
        }, 10, 3);

        add_filter('timber_user_get_meta_field', function ($value, $id, $field_name) {
            return carbon_get_user_meta($id, $field_name);
        }, 10, 3);

        add_filter('timber_comment_get_meta_field', function ($value, $id, $field_name) {
            return carbon_get_comment_meta($id, $field_name);
        }, 10, 3);
    }

}
