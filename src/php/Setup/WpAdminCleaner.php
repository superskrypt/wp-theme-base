<?php
namespace Superskrypt\WpThemeBase\Setup;

class WpAdminCleaner {

   public static function hideTemplateSelection() {
    	add_action("admin_footer", function() {
    		?>
		    <script type="text/javascript">
		        jQuery(function ($) {
		            $('#page_template').prop('disabled', 'true').css('display', 'none').prevUntil('select').css('display', 'none');
		            $('select[name="page_template"]').prop('disabled', 'true').parents('label').css('display', 'none');
		        });
		    </script>
    		<?php
    	}, 11);   
    }

 

    public static function themeSetup($options) {


	}

}
