<?php
namespace Superskrypt\WpThemeBase\Setup;

class WpAdminConfig {


	public static function scriptsToAdminPanel( $scripts ) {

        add_action( 'admin_enqueue_scripts', function() use ($scripts) {

			$path_to_js = get_template_directory_uri() . '/js/';

			foreach ( $scripts as $script ) {

				$handle = $script['handle'];
				$src = isset($script['src']) ? $script['src'] : (isset($script['file_name']) ? $path_to_js . $script['file_name'] . '.js' : $path_to_js . $script['handle'] . '.js');
				$deps = isset($script['deps']) ? $script['deps'] : [];
				$ver = isset($script['src']) ? '' : WpThemeBase::getVersionEnvironment();
				$in_footer = isset($script['in_footer']) ? $script['in_footer'] : true;

				wp_enqueue_script($handle, $src.$ver, $deps, NULL, $in_footer);

			}

        } );

    }

    public static function stylesToAdminPanel( $styles ) {

        add_action( 'admin_enqueue_scripts', function() use ($styles) {

			$path_to_css = get_template_directory_uri() . '/css/';

			foreach ( $styles as $style ) {

				$handle = $style['handle'];
				$src = isset($style['src']) ? $style['src'] : (isset($style['file_name']) ? $path_to_css . $style['file_name'] . '.css' : $path_to_css . $style['handle'] . '.css');
				$deps = isset($style['deps']) ? $style['deps'] : [];
				$ver = isset($style['src']) ? '' : WpThemeBase::getVersionEnvironment();
				$media = isset($style['media']) ? $style['media'] : 'all';

				wp_enqueue_script($handle, $src.$ver, $deps, NULL, $media);

			}

        } );

	}

	public static function loadFaviconOnAdminPages() {
		add_action('login_head', array(__CLASS__, 'addAdminFavicon'));
		add_action('admin_head', array(__CLASS__, 'addAdminFavicon'));
	}

	public static function addAdminFavicon() {
		printf('<link rel="icon" href="%s" />', get_stylesheet_directory_uri() . '/../images/favicons/favicon.png');
	}

}
