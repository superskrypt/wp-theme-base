<?php
namespace Superskrypt\WpThemeBase;

use Superskrypt\WpThemeBase\Setup\ThemeCreator;
use Superskrypt\WpThemeBase\Setup\TimberCarbonFieldsIntegration;
use Superskrypt\WpThemeBase\Setup\WpAdminCleaner;
use Superskrypt\WpThemeBase\Setup\WpAdminConfig;
use Superskrypt\WpThemeBase\Setup\WpCleaner;

class WpThemeBase {

	private const DEFAULT_OPTIONS = array(
		'menu_pages_to_top' => true,
		'themeSetup' => array(),
		'admin_styles' => array(),
		'admin_scripts' => array(),
		'styles' => array(),
		'scripts' => array(),
		'timber_carbonfields' => true,
		'page_title_scheme' => true,
	);
	
	public static function setupTheme( $options = array() ) {

		$settings = array_merge( self::DEFAULT_OPTIONS, $options );

		WpAdminConfig::stylesToAdminPanel( $settings['admin_styles'] );
		WpAdminConfig::scriptsToAdminPanel( $settings['admin_scripts'] );
		WpAdminConfig::loadFaviconOnAdminPages();

		ThemeCreator::registerScripts( $settings['scripts'] );
		ThemeCreator::registerStyles( $settings['styles'] );

		WpCleaner::init();

		if ($settings['timber_carbonfields']) TimberCarbonFieldsIntegration::init();
		if ($settings['page_title_scheme']) ThemeCreator::addPageTitleSchemeFilter();

		self::setFaviconUrl();

    }

	public static function getVersion() {
		$fileString = file_get_contents(get_template_directory().'/style.css');
		preg_match('/\d+(\.\d+)*/', $fileString, $m);

    	return $m[0];
	}

	public static function getVersionEnvironment() {
		return defined('ENVIRONMENT') && ENVIRONMENT == 'prod' ? WpThemeBase::getVersion() : rand();
	}

	public static function setFaviconUrl() {
		add_filter( 'get_site_icon_url', 
			function () {
				return get_template_directory_uri() . "/../images/favicons/favicon.ico";
			}
		);
	}

}
